#include <image_pixel_structures.h>

struct image image_create(uint32_t width, uint32_t height) {
    struct image img = {width, height, malloc(sizeof(struct pixel) * height*width)};
    return img;
}

void image_destroy(struct image* image){
    free(image->data);
    image->height=0;
    image->width=0;
}
