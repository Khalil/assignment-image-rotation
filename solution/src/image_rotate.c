#include <image_pixel_structures.h>

struct image rotate(struct image const source) {
    struct image image = image_create(source.height, source.width);
    for (size_t i = 0; i < source.width; i++) {
        for (size_t j = 0; j < source.height; j++) {
            image.data[j+i*image.width] = source.data[source.width*source.height-source.width*(j+1)+i];
        }
    }
    return image;
}
