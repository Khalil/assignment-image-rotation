#include <bmp.h>
#include <image_pixel_structures.h>





static uint8_t padding_count(uint32_t width) {
    return (4 - ((width * 3) % 4)) % 4;
}



struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed));



static struct bmp_header create_bmp_header(uint32_t width, uint32_t height) {
    struct bmp_header bmpHeader = {
            BFTYPE,
            width * height * 3 + B0FFBITS + width * padding_count(width) + 54,
            BFRESERVED,
            B0FFBITS,
            BISIZE,
            width,
            height,
            BIPLANES,
            BIBITCOUNT,
            BICOMPRESSION,
            width * height * 3,
            BIX,
            BIY,
            BICLRSUSED,
            BICLRIMPORTANT};
    return bmpHeader;
}
static bool bmp_type_is_ok(struct bmp_header bmpHeader) {
    return bmpHeader.bfType == BFTYPE;
}

static bool bmp_bits_is_ok(struct bmp_header bmpHeader) {
    return bmpHeader.biBitCount == BIBITCOUNT && bmpHeader.biCompression == BICOMPRESSION;
}


static bool bmp_header_is_ok(struct bmp_header bmpHeader) {
    return bmpHeader.bfileSize == bmpHeader.biSizeImage + bmpHeader.bOffBits;
}


enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header bmpHeader;
    if(fread(&bmpHeader, sizeof(bmpHeader), 1, in)!=1){
        fprintf(stderr,"Reading was failed");
        return READ_ERROR;
    }
    if (!bmp_type_is_ok(bmpHeader)) {
        return READ_INVALID_SIGNATURE;
    }
    if (!bmp_bits_is_ok(bmpHeader)) {
        return READ_INVALID_BITS;
    }
    if (!bmp_header_is_ok(bmpHeader)) {
        return READ_INVALID_HEADER;
    }
    const uint8_t padding = padding_count(bmpHeader.biWidth);
    *img= image_create(bmpHeader.biWidth,bmpHeader.biHeight);
    uint8_t police[3];
    for (size_t i = 0; i < img->height; i++) {
        if(fread(img->data+i*img->width, sizeof(struct pixel), img->width, in)!=img->width || fread(police, padding, 1, in)!=1){
            fprintf(stderr,"Reading was failed");
        }
    }
    return READ_OK;

}
enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header bmpHeader = create_bmp_header(img->width, img->height);
    if(fwrite(&bmpHeader, sizeof(bmpHeader), 1, out)!=1){
        fprintf(stderr,"Writing was failed");
        return WRITE_ERROR;
    }
    uint8_t padding = padding_count(img->width);
    uint8_t police[3]={0};
    for (size_t i = 0; i < bmpHeader.biHeight; i++) {
        if(fwrite(img->data+i*img->width, sizeof(struct pixel), img->width, out)!=img->width || fwrite(police, padding, 1, out)!=1) {
            fprintf(stderr,"Writing was failed");
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
