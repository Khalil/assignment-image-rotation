#include <bmp.h>
#include <image_rotate.h>
int main( int argc, char** argv ) {

    if (argc != 3) {
        printf("Invalid number of arguments!");
        return -1;
    }

    FILE *input_file = fopen(argv[1], "rb");
    if(!input_file){
        fprintf(stderr,"Input file is invalid");
        return -1;
    }
    struct image image;
    from_bmp(input_file, &image);
    struct image image_rotated = rotate(image);
    FILE *output_file = fopen(argv[2], "wb");
    if(!output_file){
        fprintf(stderr,"Output file is invalid");
        return -1;
    }
    to_bmp(output_file, &image_rotated);
    image_destroy(&image);
    image_destroy(&image_rotated);
    fclose(input_file);
    fclose(output_file);
    return 0;
}
