#pragma once
#include <image_pixel_structures.h>
#include <read_statuses.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <write_statuses.h>

#define BFTYPE 0x4d42
#define BFRESERVED 0
#define B0FFBITS 54
#define BISIZE 40
#define BIPLANES 1
#define BIBITCOUNT 24
#define BICOMPRESSION 0
#define BIX 2834
#define BIY 2834
#define BICLRSUSED 0
#define BICLRIMPORTANT 0

enum read_status from_bmp(FILE *in, struct image *img);

enum write_status to_bmp(FILE *out, struct image const *img);

