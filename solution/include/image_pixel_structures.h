#pragma once
#include <inttypes.h>
#include <malloc.h>

struct pixel { uint8_t b, g, r; };


struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image image_create(uint32_t width, uint32_t height);
void image_destroy(struct image* image);

